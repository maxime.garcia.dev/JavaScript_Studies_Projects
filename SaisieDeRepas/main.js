"use strict";

/**
 * EVALUATION JAVASCRIPT: MAXIME GARCIA
 */

/***********************
 *    VARIABLES
 **********************/
let addButton;
let menu;

/***********************
 *     FUNCTIONS
 **********************/

/**
 * ajoute un plat à notre menu si et seulement si celui ci n'existe pas avant de renvoyer vers la fonction d'affichage
 * @param {*} event 
 */
function addMeal(){

    //récupération du texte que l'on a tapé au préalable
    const newMeal = document.querySelector("input[type='text']").value;
    
    //variable de vérification de doublons
    let check = false;
    //parcours de la liste à la recherche de doublon on set check a true si l'on trouve un plat déjà existant
    menu.forEach(meal => {
        // On rend la vérification insensible à la case et l'on enleve les espaces avant et après.
        if (meal.toLowerCase().trim() == newMeal.toLowerCase().trim()){
            check = true;
        }
    });
    
    //affichage d'une pop-up avec le message adéquat.
    //Si la chaine de caractère est vide.
    if (newMeal.trim() == ""){
        alert ("😱 PLease enter a meal before add");
    }
    //Si l'on a trouvé un doublon.
    else if (check == true){
        alert("⛔ this meal already exist");
    }
    //Si le plat est valide. 
    else {
        menu.push(newMeal.trim());
        alert(`✅ New meal: ${newMeal.trim()} added! 😄`);
    }
   
    //On reset le formulaire afin d'avoir un input vide après avoir ajouter un plat.
    document.querySelector("form").reset();
    //On affiche le nouveau menu afin de mettre a jour les plats et le compteur.
    displayMenu();

}

/**
 * affiche le menu.
 */
function displayMenu(){
    // Création d'un titre et d'une liste.
    const UL = document.createElement("ul");
    const H3 = document.createElement("h3");
    let meals = document.querySelector("#meals");

    // on ajoute integre au titre un compteur = à la taille de notre liste de plat.
    H3.insertAdjacentHTML("beforeend", `Il y a ${menu.length} plats au menu:`);
    //On parcours notre liste de plat et pour chacun d'eux on rajoute une li contenant le plat dans la ul
    menu.forEach(meal => {
        UL.insertAdjacentHTML(
            "beforeend",
            `<li>
                ${meal}
            </li>`
        )
    });
    //avant d'ajouter notre h3 et notre ul à l'html on vide d'abord la div ciblé de son ancien contenu
    meals.innerHTML = "";
    //ajout du titre dans la div contenant l'id "meals"
    meals.appendChild(H3);
     //ajout de la ul dans la div contenant l'id "meals"
    meals.appendChild(UL);
}

/***********************
 *  PRINCIPAL CODE
 **********************/

document.addEventListener("DOMContentLoaded", function(){
    addButton =  document.querySelector("input[type='button']");
    //menu de départ
    menu = ["Couscous", "Boeuf carotte", "Poulet Tandori", "Lasagnes", "Ramen", "Burger", "Pizza", "Burritos", "Empenadas", "Paella" ];
    //affichage du menu de départ
    displayMenu();
    //on set une gestion d'évenement sur le bouton ajouter.
    addButton.addEventListener("click", addMeal);
});