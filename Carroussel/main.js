"use strict";

/*****************
 * FUNCTIONS
 *****************/

/**
 * Affiche/cache la ul contenant les boutons du carroussel
 */
function wrapMenuOnClick(){
    console.log("squiker");
    document.querySelector("ul").classList.toggle("hide");
    if (!document.querySelector("ul").classList.contains("hide")){
      document.querySelector("#toolbar-toggle>i").classList.replace("fa-arrow-right","fa-arrow-down");
    } else {
      document.querySelector("#toolbar-toggle>i").classList.replace("fa-arrow-down","fa-arrow-right");
    }
}

function refreshSlider(){
  console.log("chargement photo");
  console.log(photos[state.index].src);
  document.querySelector("#slider img").src = "images/"+photos[state.index].src;
  document.querySelector("#slider figcaption").textContent = photos[state.index].legend;
}

function nextPhotos(){
  if (state.index == photos.length - 1){
    state.index = 0;
  } else {
    state.index++;
  }
  refreshSlider();
}

function previousPhotos(){
  if (state.index == 0){
    state.index = photos.length - 1;
  } else {
    state.index--;
  }
  refreshSlider();
}

function getRandomInteger(min, max){
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function randomPhotos(){
  let temp = getRandomInteger(0, photos.length - 1);
  while (temp == state.index){
    temp = getRandomInteger(0, photos.length - 1);
  }
  state.index = temp;
  refreshSlider();
}

function toggleCarroussel(){
  const icon = document.querySelector("#slider-toggle>i");
  const btn = document.querySelector("#slider-toggle");
  icon.classList.toggle("fa-play");
  icon.classList.toggle("fa-pause");
  if (document.querySelector("#slider-toggle>i").classList.contains("fa-pause")){
    state.timer = window.setInterval(nextPhotos, 2000);
    btn.title = "Arrêter le Carroussel";
  } else {
    window.clearInterval(state.timer);
    state.timer = null;
    btn.title = "Démarrer le carrousel";
  }
}

 /*******************
  * CODE PRINCIPALE
  *******************/
const photos = [
  { src: "1.jpg", legend: "Frères pandas" },
  { src: "2.jpg", legend: "Yoga on the top" },
  { src: "3.jpg", legend: "Lever de soleil" },
  { src: "4.jpg", legend: "Ciel étoilé" },
  { src: "5.jpg", legend: "Tea time" },
  { src: "6.jpg", legend: "Ca va péter le bide" },
];

let state = new Object();

document.addEventListener("DOMContentLoaded", function(){
  state.index = 0;
  state.timer = null;
  refreshSlider();
  document.querySelector("#toolbar-toggle").addEventListener("click", wrapMenuOnClick);
  document.querySelector("#slider-next").addEventListener("click", nextPhotos);
  document.querySelector("#slider-previous").addEventListener("click", previousPhotos);
  document.querySelector("#slider-random").addEventListener("click", randomPhotos);
  document.querySelector("#slider-toggle").addEventListener("click", toggleCarroussel);
})
