"use strict";

/*****************
 *   FUNCTIONS
 ****************/

/**
 * Compte le nombres d'images séléctionnées
 */
function counter() {
  let nbSelected = document.querySelectorAll("li.selected").length;
  document.querySelector("#total em").textContent = nbSelected;
}
/**
 * Selectionne toutes les images et met a jour le compteur d'images selectionnées
 */
function onClickSelect() {
  console.log("clicked");
  PHOTOS.forEach((img) => img.classList.add("selected"));
  counter();
}

/**
 * Deselectionne toutes les images et met a jour le compteur d'images selectionnées
 */
function onClickDeselect() {
  console.log("clickos");
  PHOTOS.forEach((img) => img.classList.remove("selected"));

  counter();
}

function onToggle() {
  console.log(this.dataset.mode);
  if (this.dataset.mode == "select") {
    PHOTOS.forEach((img) => img.classList.add("selected"));
    this.dataset.mode = "deselect";
    this.textContent = "❌ Tout déselectionner";
  } else {
    PHOTOS.forEach((img) => img.classList.remove("selected"));
    this.dataset.mode = "select";
    this.textContent = "✅ Tout séléctionner";
  }
  counter();
}
/**
 * Permet de selectionner une image en cliquant dessus et met ajour le compteur
 */
function selectOne() {
  console.log("clic2");
  this.classList.toggle("selected");

  counter();
}
/******************
 * CODE PRINCIPALE
 ******************/

const PHOTOS = document.querySelectorAll(".photo-list li");

document.querySelector("#selectAll").addEventListener("click", onClickSelect);
document.querySelector("#deselectAll").addEventListener("click", onClickDeselect);
document.querySelector("#toggle").addEventListener("click", onToggle);
/**
   * for
   
    for (let i = 0; i < PHOTOS.length; i++){
        PHOTOS[i].addEventListener("click", selectOne);
    }
    */
/**
    * for...of
    
    for (let img of PHOTOS){
        img.addEventListener("click", selectOne);
    }
    */
/**
     * foreach (2 écritures possibles)
     
    PHOTOS.forEach(function(img){
      img.addEventListener("click", selectOne);  
    });
    */
PHOTOS.forEach((img) => img.addEventListener("click", selectOne));
