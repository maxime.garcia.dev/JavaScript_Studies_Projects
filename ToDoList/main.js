"use strict";

import { save, load } from "./storage.js";
/**********************
 *     VARIABLES
 *********************/
let form;
let addTask;
const STORAGE = "todoList";

/**********************
 *     FUNCTIONS
 *********************/

function showTaskForm(){
    if(!this.dataset.index){
        form.dataset.mode = "add";
        form.reset();
        form.classList.remove("hide");
    } else {
        let tasklist = load(STORAGE);
        form.dataset.mode ="edit";
        document.querySelector("#name").value= tasklist[this.dataset.index].name;
        document.querySelector("#lvl").value = tasklist[this.dataset.index].level;
        document.querySelector("#description").value = tasklist[this.dataset.index].description;
        form.classList.remove("hide");
    }
}

function displayTask(){
    let taskList = load(STORAGE);

    const UL = document.createElement("ul");

    taskList.forEach((task, index) => {
        UL.insertAdjacentHTML(
            "beforeend",
            `<li>
                <a class="task ${task.level == 100 ? "barre" : ""}" data-index="${index}">${task.name} - ${task.level}%</a><i class="fa fa-trash" data-index="${index}"></i>
            </li>`);
    });
    document.querySelector("#todo").innerHTML = "";
    document.querySelector("#todo").appendChild(UL);
    document.querySelectorAll("#todo .task").forEach((a) => a.addEventListener("click", showDetail));
    document.querySelectorAll("#todo i").forEach((i) => i.addEventListener("click", deleteTask));
}

function saveTask(event){
    event.preventDefault();
    let taskList = load(STORAGE);

    const task = {
        name: document.querySelector("#name").value,
        level: document.querySelector("#lvl").value,
        description: document.querySelector("#description").value,
    };

    if (document.querySelector("#task-form").dataset.mode == "add"){
        taskList.push(task);
    } else {
        let index =  document.querySelector("#task-details a").dataset.index;
        taskList.splice(index, 1, task);
    }
    save(STORAGE, taskList);
    form.classList.add("hide");
    document.querySelector("#task-details").classList.add("hide");
    displayTask();
}

function showDetail(){
    let taskList = load(STORAGE);
    let index = this.dataset.index;
    let task = taskList[index];
    if (task.level == 100) {
        document.querySelector("#task-details h3").innerHTML = `<i class="fa fa-check"></i> ${task.name} - ${task.level}%`;
    } else {
        document.querySelector("#task-details h3").innerHTML = `${task.name} - ${task.level}%`;
    }
    document.querySelector("#task-details p").innerHTML = task.description;
    document.querySelector("#task-details a").dataset.index = index;
    document.querySelector("#task-details").classList.remove("hide");
}

function deleteAllTasks(){
    localStorage.clear(STORAGE);
    //ou save(Storage, []);
    displayTask();
}

function deleteTask(){
    let taskList = load(STORAGE);
    let index = this.dataset.index;
    taskList.splice(index, 1);
    save(STORAGE, taskList);
    displayTask();
}

/***********************
 *   CODE PRINCIPALE
 **********************/

document.addEventListener("DOMContentLoaded", function(){
    displayTask();
    form = document.querySelector("#task-form");
    addTask = document.querySelector("#add-task i");

    addTask.addEventListener("click", showTaskForm);
    form.addEventListener("submit", saveTask);
    document.querySelector("#task-details a").addEventListener("click", showTaskForm);
    document.querySelector("#clear-todo i").addEventListener("click", deleteAllTasks);
});