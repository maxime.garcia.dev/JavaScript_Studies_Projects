"use strict";

export function save(key, list){
    window.localStorage.setItem(key, JSON.stringify(list));
}

export function load(key){
    const datas = window.localStorage.getItem(key);
    return datas == null ? [] : JSON.parse(datas);
}