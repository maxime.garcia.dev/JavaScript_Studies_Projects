"use strict";

import { getRandomInteger, getRandomRGBA } from "./utilities.js"

import { getRandomInteger } from "./utilities";

/******************
 *   FUNCTIONS
 *****************/
function onMouseClick(event){
    console.log(event.offsetX, event.offsetY);
    let radius = getRandomInteger(10, 50);
    let color = getRandomRGBA();
    let disk = new Disk();
    disk.setColor(color);
    disck.setRadius(radius);
    disk.setLocation(event.offsetX, event.offsetY);
    console.log(disk);
}

/**********************
 *  CODE PRINCIPAL
 *********************/
document.addEventListener("DOMContentLoaded", function(){
    document.querySelector("#masterpiece").addEventListener("click", onMouseClick);
});