"use strict";

class Disk{
    color;
    radius;
    location;

    constructor(settings = {}){
        const defaultValues = {
            color: "black",
            radius: 20,
            location: {x: 0, y: 0}
        };

        for (let prop in this){
            this[prop] = settings[prop] ? settings[prop]: defaultvalues[prop];
        }
    }

    getColor(){
        return this.color;
    }
    setColor(color){
        this.color = color;
    }

    getRadius(){
        return this.radius;
    }

    setRadius(radius){
        this.radius = radius;
    }

    getLocation(){
        return this.location;
    }

    setLocation(x, y){
        this.location = { x, y };
    }

    draw (event){
        
    }
}