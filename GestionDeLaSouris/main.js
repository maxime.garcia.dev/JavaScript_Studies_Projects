"use strict";


/*************
 * FUNCTIONS
 ************/
function onClickButton() {
  RECTANGLE.classList.toggle("hide");
  // if(RECTANGLE.classList.contains("hide")){
  //     RECTANGLE.classList.remove("hide");
  // }else {
  //  RECTANGLE.classList.add("hide");
}

/**
 * Add la classe '.important' a la classe '.rectangle'
 * Passe le fond du rectangle en rouge.
 */
function onMouseOver() {
  RECTANGLE.classList.add("important");
}

/**
 * Remove les classes '.important' et '.good' de la classe '.rectangle'
 * Repasse le fond en bleu lorsque la souris sort du rectangle
 */
function onMouseOut(){
    RECTANGLE.classList.remove("important", "good");
}

/**
 * Add la classe '.good' a la classe '.rectangle'
 * Passe le fond en beige après un double click sur le rectangle
 */
function onDoubleClick(){
    RECTANGLE.classList.add("good");
}

/****************
 * CODE PRINCIPAL
 ****************/
const RECTANGLE = document.querySelector(".rectangle");

document
  .querySelector("#toggle-rectangle")
  .addEventListener("click", onClickButton);

RECTANGLE.addEventListener("mouseover", onMouseOver);
RECTANGLE.addEventListener("mouseout", onMouseOut);
RECTANGLE.addEventListener("dblclick", onDoubleClick);